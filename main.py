# -*- coding: utf-8 -*-
"""
    这是一个示例 Python 脚本。
    用于从录波仪上蹿记录数据到PC
    PC目前是windows7~windows10操作系统
    可以方便地移植到其它操作系统
"""
import sys

from PyQt5 import QtWidgets
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMainWindow, QMessageBox
from serial import Serial
from serial.tools.list_ports import comports

from crc import crc16
from ui4me import Ui_hu_ui

ONESECOND = 50


class MyMain(QMainWindow, Ui_hu_ui):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.main_init1()

    def main_init1(self):
        self.ser = Serial()
        self._find_port()
        self.is_com_open = False
        self._close_ser()
        # -----------------------
        self.tmrSer = QTimer()
        self.tmrSer.setInterval(20)
        self.tmrSer.timeout.connect(self.timeout_event)
        self.tmrSer.start()
        self.read_time = 0
        self.write_time = 0
        # -----------------------
        self.combtn.clicked.connect(self.opencom)
        self.scanbtn.clicked.connect(self.scancom)
        self.read_btn.clicked.connect(self.read_list)
        self.write_btn.clicked.connect(self.read_data)
        self.btnOpen.clicked.connect(self.testbtn)

    # --------------------------------------
    def opencom(self):
        if not self.is_com_open:
            self._open_ser()
        else:
            self._close_ser()

    def _find_port(self):
        port_list = list(comports())
        self.combox.clear()
        self.combox.addItems([port[0] for port in port_list])

    def scancom(self):
        self._close_ser()
        self._find_port()

    def _close_ser(self):
        self.ser.close()
        self.is_com_open = False
        self._dis_ser_par(False)

    def _open_ser(self):
        try:
            self.ser.port = self.combox.currentText()
            self.ser.baudrate = "115200"
            self.ser.parity = 'N'
            self.ser.bytesize = 8
            self.ser.stopbits = 1
            self.ser.open()
        except:
            self._close_ser()
            QMessageBox.about(self, '提示', '打开串口出错！')
            return

        self.is_com_open = True
        self._dis_ser_par(True)

    def _dis_ser_par(self, val):
        if val:
            # 三个btn使能操作
            self.write_btn.setEnabled(True)
            self.read_btn.setEnabled(True)
            # self.wm_btn.setEnabled(True)
            # 三个通知
            self.status_w.setText("写通讯状态： 串口已经开启，可以进行通讯")
            self.status_r.setText("读通讯状态： 串口已经开启，可以进行通讯")
            # self.status_m.setText("读通讯状态： 串口已经开启，可以进行通讯")
            # other
            self.combtn.setText("关闭串口")
            news = ' %s.%s.%s.%d.%d' % (self.ser.port, self.ser.baudrate, self.ser.parity,
                                        self.ser.bytesize, self.ser.stopbits)
        else:
            # 三个btn使能操作
            self.write_btn.setEnabled(False)
            self.read_btn.setEnabled(False)
            # self.wm_btn.setEnabled(False)
            # 三个通知
            self.status_w.setText("写通讯状态： 串口未开启，不能进行通讯")
            self.status_r.setText("读通讯状态： 串口未开启，不能进行通讯")
            # self.status_m.setText("读通讯状态： 串口未开启，不能进行通讯")
            # other
            self.combtn.setText("打开串口")
            news = "  串口关闭状态"

        self.comtip.setText(news)

    def testbtn(self):
        # str1 = os.getcwd()

        str2 = self.cmbList.currentText()
        str1 = str2.replace('/', '-')
        str2 = str1.replace(':', '_')
        str1 = str2 + ".csv"

        self.f = open(str1, 'w')
        self.f.write(str1)
        self.f.close()

    # ---------timer-----------------------
    def timeout_event(self):
        if not self.is_com_open:
            return
        # ----------------------
        try:
            num = self.ser.inWaiting()
        except:
            self._close_ser()
            QMessageBox.about(self, '提示', '串口丢失，请检查端口')
            return

        # -----read-list----------------
        if self.read_time > 0:
            self.read_time -= 1
            if self.read_time == 0:
                if 0 == self.read_no:
                    self.status_r.setText("读通讯状态： 通讯超时，请检查通讯参数和地址设置")
                return
            else:
                if num > 0:
                    bytes1 = self.ser.readline()
                    str2 = bytes1.decode(encoding="gbk")
                    self.cmbList.addItem(str2.strip())
                    self.status_r.setText("读通讯状态： 读通讯正常")
                    self.read_no += 1

        # -----------read data (write)---------------------
        if self.write_time > 0:
            if num > 0:
                bytes1 = self.ser.read(num)
                # str2 = bytes1.decode(encoding="gbk")
                str2 = bytes1.decode()
                self.f.write(str2)
                self.read_no += 1
            else:
                self.write_time -= 1
                if self.write_time == 0:
                    if 0 == self.read_no:
                        self.status_w.setText("写通讯状态： 通讯超时，请检查通讯参数和地址设置")
                    else:
                        self.status_w.setText("读记录状态： 通讯正常")

                    self.f.close()

    def _close_ser(self):
        self.ser.close()
        self.is_com_open = False
        self._dis_ser_par(False)

    def read_list(self):
        self.cmbList.clear()
        dat = [0x01, 0x23, 0x00, 0x10, 0x00, 0x01]
        check = crc16()
        check.createarray(dat)
        send = bytes([a for a in dat])
        self.rx_buff = []
        self.ser.write(send)
        self.txtRcv.setText(' '.join([('%02X' % i) for i in send]))
        self.read_time = ONESECOND
        self.read_no = 0

    def read_data(self):
        dat = [0x01, 0x30, 0x00, 0x00, 0x00, 0x03, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00]
        dat[8] = self.cmbList.currentIndex()
        if 0 > dat[8]:
            self.status_w.setText("请选择数据纪录!")
            return

        if self.rdBCurve.isChecked():  # 波形图（每周波几百点）
            dat[10] = 0x01

        if self.rdBOther.isChecked():  # 第三方（Scope）文件格式
            dat[12] = 0x01

        check = crc16()
        check.createarray(dat)
        send = bytes([a for a in dat])
        self.rx_buff = []
        self.ser.write(send)
        self.txtRcv.setText(' '.join([('%02X' % i) for i in send]))
        self.write_time = ONESECOND
        self.read_no = 0
        self.status_w.setText("请稍候......")
        # ---format file name-------------------
        str2 = self.cmbList.currentText()
        str1 = str2.replace('/', '-')
        str2 = str1.replace(':', '_')
        if self.rdBCurve.isChecked():
            if self.rdBOther.isChecked():
                str1 = str2 + "D.csv"
            else:
                str1 = str2 + "C.csv"
        else:
            if self.rdBOther.isChecked():
                str1 = str2 + "B.csv"
            else:
                str1 = str2 + "A.csv"

        self.f = open(str1, 'w')


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    ui = MyMain()
    ui.show()
    sys.exit(app.exec_())
